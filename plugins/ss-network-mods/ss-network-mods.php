<?php

/*
Plugin Name: SS-Network Mods
Description: Plugin & Site Modifications
Author: SS-Network
Version: 0.1
*/

if ( ! class_exists( 'ss_network_roles' ) ) {

	class ss_network_roles {

		private $capabilities = null,
			$roles_needed = array( 'contributor' ),
			$method = null;

		function __construct() {

			// Launch checking functions after WooCommerce has been loaded and kicked off it's action to do so. Low priority.
			add_action( 'woocommerce_loaded', array( $this, 'launch' ), 10 );

			// Render vendor author views if role is permitted in $roles_needed list
			add_action( 'plugins_loaded', array( $this, 'permit_vendor_author_views' ), 20 );

		}

		public function launch() {

			/*
			 * Instantiate the WC_Install class to get role capabilities that are set upon install.
			 * The method is set as private so using the reflection class to access the private method.
			 * This should provide future proofing for if/when WooCommerce provides updates and expands their role permissions sets.
			 */

			$wci   = new WC_Install();
			$class = new \ReflectionClass( $wci );

			$this->method = $class->getMethod( 'get_core_capabilities' );
			$this->method->setAccessible( true );
			$this->capabilities = $this->method->invoke( $wci );

			// Check to see if there's WooCommerce support for a role, if not then give associated role permissions set by the WC_Install class.
			foreach ( $this->roles_needed as $role ) {
				$this->check_role_support( $role );
			}
		}

		public function permit_vendor_author_views() {

			// Check if user is logged in and validate role
			if ( ! is_user_logged_in() ) {
				return false;
			}

			$user  = wp_get_current_user();
			$roles = (array) $user->roles;

			$validate_roles = count(array_intersect( $this->roles_needed, $roles ));

			if ( $validate_roles > 0 ) {

				$venview = new WCV_Product_Meta;

				add_post_type_support( 'product', 'author' );

				add_action( 'add_meta_boxes', array( $venview, 'change_author_meta_box_title' ) );
				add_action( 'wp_dropdown_users', array( $venview, 'author_vendor_roles' ), 0, 1 );
				add_filter( 'manage_product_posts_columns', array( $venview, 'vendor_column_quickedit' ) );

				return true;
			}

			return false;
		}

		private function check_role_support( $check_role ) {

			$role = get_role( $check_role );

			if ( $role->capabilities['edit_others_products'] ) {
				// Remove role support, if needed.
				$this->remove_role_support( false, $check_role );

				return true;
			} else {
				$this->add_role_support( true, $check_role );

				return false;
			}
		}

		private function add_role_support( $run = true, $role ) {

			if ( ! $run ) {
				return false;
			}

			global $wp_roles;

			foreach ( $this->capabilities as $cap_group ) {
				foreach ( $cap_group as $cap ) {
					$wp_roles->add_cap( $role, $cap );
				}
			}

			$wp_roles->remove_cap( $role, 'manage_woocommerce' );

			return true;
		}

		private function remove_role_support( $run = false, $role ) {

			if ( ! $run ) {
				return false;
			}

			global $wp_roles;

			foreach ( $this->capabilities as $cap_group ) {
				foreach ( $cap_group as $cap ) {
					$wp_roles->remove_cap( $role, $cap );
				}
			}

			$wp_roles->remove_cap( $role, 'manage_woocommerce' );

			return true;
		}
	}

	$ss_network['roles'] = new ss_network_roles;
}


