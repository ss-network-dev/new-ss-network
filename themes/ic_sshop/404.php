<?php
/**
 * The template for displaying 404 results pages.
 */

get_header(); ?>

<main id="main" class="main" role="main">
    <div class="wrap">
        <div class="sidebar"><?php dynamic_sidebar( 'shop-sidebar' ); ?></div>
        <div class="woo-content">
            <h1 class="page-title">Page Not Found. Try Searching our Shit.</h1>
            <?php get_search_form(); ?>
        </div>
    </div>
</main>

<?php
get_footer();