	<footer id="footer" class="site-footer">
		<div class="wrap">
			<?php wp_nav_menu( array(
				'container' 		=> 'nav',
				'container_class' 	=> 'menu-container footer-menu-container',
				'theme_location' 	=> 'footer-menu',
			) ); ?>
		</div>
	</footer>

	<div class="off-canvas-container">
		<div class="button-container">
			<button id="off-canvas-close" class="off-canvas-close">
				<span class="button-text">Close</span>
				<span class="fas fa-times"></span>
			</button>
		</div>

		<?php wp_nav_menu( array(
			'container' 		=> 'nav',
			'container_class' 	=> 'menu-container off-canvas-menu-container',
			'theme_location' 	=> 'off-canvas-menu',
		) ); ?>
	</div>

	<!-- No touch! -->
	<?php wp_footer(); ?>

</body>
</html>