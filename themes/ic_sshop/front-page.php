<?php

get_header();
?>
<main id="site-main" class="main">
	<div class="wrap">
		<div class="home-slider">
			<!-- Slider Loop -->
			<?php
			$slider_args = array( 'post_type' => 'ss_slides', 'posts_per_page' => -1 );

			$slides = new WP_Query( $slider_args );

			if( $slides->have_posts() ) {
				while( $slides->have_posts() ) {
					$slides->the_post();

					the_post_thumbnail( 'full' );
				}

				wp_reset_postdata();
			}
			?>
		</div>

		<?php wc_print_notices(); ?>

<!-- Hot Products Loop -->
<?php
	$hot_args = array(
		'post_type' 		=> 'product',
		'posts_per_page' 	=> 10,
		'product_tag'		=> 'hot',
	);

	$hot = new WP_Query( $hot_args );

	if( $hot->have_posts() ) {
		?>
		<div class="section hot">
			<h2 class="section-title"><?php esc_html_e( '🔥 Hot Shit 🔥', 'sshop' ) ?></h2>
			<div class="front-products">
			<?php
			while( $hot->have_posts() ) {
				$hot->the_post();

				get_template_part( 'template-parts/product', 'front' );
			}

			wp_reset_postdata();
			?>
			</div>
		</div>
		<?php
	}
?>

<!-- Main Products Loop -->
<?php
	$product_args = array(
		'post_type' => 'product',
		'posts_per_page' => 25,
	);

	$products = new WP_Query( $product_args );

	if( $products->have_posts() ) {
		?>
		<div class="section">
			<h2 class="section-title"><?php esc_html_e( 'This Shit is New!', 'sshop' ) ?></h2>
			<div class="front-products">
			<?php
			while( $products->have_posts() ) {
				$products->the_post();

				get_template_part( 'template-parts/product', 'front' );
			}

			wp_reset_postdata();
			?>
			</div>
		</div>
		<?php
	}
?>

	</div>
</main>
<?php
get_footer();