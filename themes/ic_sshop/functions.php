<?php

// General functionality
require get_stylesheet_directory() . '/inc/general/scripts-styles.php';
require get_stylesheet_directory() . '/inc/general/theme-supports.php';
require get_stylesheet_directory() . '/inc/general/image-sizes.php';
require get_stylesheet_directory() . '/inc/general/menus.php';
require get_stylesheet_directory() . '/inc/general/sidebars.php';


// Custom Post Types needed by the theme
require get_stylesheet_directory() . '/inc/custom-post-types/custom-post-types.php';


// Customizer
require get_stylesheet_directory() . '/inc/customizer/register.php';
require get_stylesheet_directory() . '/inc/customizer/css.php';


// WooCommerce
require get_stylesheet_directory() . '/inc/woocommerce/cart-header.php';
require get_stylesheet_directory() . '/inc/woocommerce/shop-thumbnail.php';
require get_stylesheet_directory() . '/inc/woocommerce/single-product-tabs.php';
require get_stylesheet_directory() . '/inc/woocommerce/shop-title.php';
require get_stylesheet_directory() . '/inc/woocommerce/ajax-add-to-cart.php';


// ACF
require get_stylesheet_directory() . '/inc/acf/options-pages.php';
