<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- No touch! -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="top-bar">
		<?php 
			wp_nav_menu( array(
				'container' 		=> 'nav',
				'container_class' 	=> 'menu-container sites-menu-container',
				'theme_location' 	=> 'sites-menu',
			) ); 

			if ( is_user_logged_in() ) {
				wp_nav_menu( array(
					'container' 		=> 'nav',
					'container_class' 	=> 'menu-container top-bar-menu-container',
					'theme_location' 	=> 'top-bar-menu-logged-in',
				) ); 
			} else {
				wp_nav_menu( array(
					'container' 		=> 'nav',
					'container_class' 	=> 'menu-container top-bar-menu-container',
					'theme_location' 	=> 'top-bar-menu',
				) ); 
			}
		?>
	</div>

	<header id="header" class="site-header">
		<div class="title-container">
			<a href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
				<img alt="<?php echo esc_html( get_bloginfo( 'url' ) ); ?>" src="<?php echo esc_url( get_field( 'logo', 'options' ) ); ?>">
			</a>
		</div>

		<?php wp_nav_menu( array(
				'container' 		=> 'nav',
				'container_class' 	=> 'menu-container header-menu-container',
				'theme_location' 	=> 'header-menu',
			) ); 
		?>

		<div class="header-buttons-container">
			<div class="cart-container">
				<a href="<?php echo wc_get_cart_url(); ?>" class="button cart-customlocation">
					<span class="fas fa-shopping-cart"></span>
					<span class="button-text"><?php echo WC()->cart->get_cart_total(); ?></span>
				</a>
			</div>

			<div class="search-container">
				<button id="search-trigger" class="search-trigger">
					<span class="fas fa-search"></span>
				</button>
				<div class="search-form-container">
					<?php get_search_form( true ); ?>
				</div>
			</div>

			<button id="off-canvas-trigger" class="menu-trigger">
				<span class="button-text">Menu</span><span class="fas fa-bars"></span>
			</button>
		</div>
	</header>