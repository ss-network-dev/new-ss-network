<?php

require get_stylesheet_directory() . '/inc/custom-post-types/class-CPT.php';

class SS_Slides extends CPT
{
	public function __construct()
	{
		$this->labels = array(
	        	'name' 			=> 'Slides',
	        	'singular_name'		=> 'Slide'
		);

		$this->settings = array(
			'labels'		=> $this->labels,
			'supports'		=> array( 'title', 'thumbnail' ),
			'public' 		=> true,
			'has_archive' 		=> true,
			'menu_icon' 		=> 'dashicons-images-alt',
			'rewrite' 		=> array( 'slug' => 'slides' ),
	    );
	}

	public function register_post_type()
	{
		register_post_type( 'ss_slides', $this->settings );
	}
}
