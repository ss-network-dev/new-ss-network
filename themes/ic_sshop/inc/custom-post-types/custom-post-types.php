<?php

// Include our CPT files
require get_stylesheet_directory() . '/inc/custom-post-types/class-SS-slides.php';


/**
 * Registers any custom post types created by the plugin.
 *
 * @since 1.0.0
 */
function register_CPTs()
{
	// Get an instance of the CPT
	$slides		= new SS_Slides();

	// Call the registration function
	add_action( 'init', $slides->register_post_type() );
}
add_action( 'init', 'register_CPTs' );