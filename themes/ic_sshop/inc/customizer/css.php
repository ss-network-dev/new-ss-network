<?php

function sshop_customizer_css() {
?>
<style type="text/css">
	body {
		background: <?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?>;

		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,7db9e8+100 */
		background: <?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?>; /* Old browsers */
		background: -moz-linear-gradient(180deg, <?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?> 0%, <?php echo get_theme_mod( 'body_background_color_2', '#F9F3ED' ); ?> 20%); /* FF3.6-15 */
		background: -webkit-linear-gradient(180deg, <?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?> 0%,<?php echo get_theme_mod( 'body_background_color_2', '#F9F3ED' ); ?> 20%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(180deg, <?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?> 0%,<?php echo get_theme_mod( 'body_background_color_2', '#F9F3ED' ); ?> 20%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo get_theme_mod( 'body_background_color_1', '#F9F3ED' ); ?>', endColorstr='<?php echo get_theme_mod( 'body_background_color_2', '#F9F3ED' ); ?>',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
	}

	.top-bar {
		color: <?php echo get_theme_mod( 'top_bar_text_color', '#000000' ); ?>;
	}

	.site-header {
		color: <?php echo get_theme_mod( 'site_header_text_color', '#FFFFFF' ); ?>;
	}

	.site-header .search-form-container {
		background: <?php echo get_theme_mod( 'search_bar_background_color', '#000000' ); ?>;
	}

	.off-canvas-container {
		background: <?php echo get_theme_mod( 'off_canvas_menu_background_color', '#000000' ); ?>;
	}

	.site-footer {
		background: <?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?>;
		color: <?php echo get_theme_mod( 'site_footer_text_color', '#FFFFFF' ); ?>;

		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,7db9e8+100 */
		background: <?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?>; /* Old browsers */
		background: -moz-linear-gradient(45deg, <?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?> 0%, <?php echo get_theme_mod( 'site_footer_background_color_2', '#000000' ); ?> 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(45deg, <?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?> 0%,<?php echo get_theme_mod( 'site_footer_background_color_2', '#000000' ); ?> 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(45deg, <?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?> 0%,<?php echo get_theme_mod( 'site_footer_background_color_2', '#000000' ); ?> 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo get_theme_mod( 'site_footer_background_color_1', '#000000' ); ?>', endColorstr='<?php echo get_theme_mod( 'site_footer_background_color_2', '#000000' ); ?>',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */

		border-color: <?php echo get_theme_mod( 'site_footer_border_color', '#999999' ); ?>;
	}

</style>
<?php
}
add_action( 'wp_head', 'sshop_customizer_css' );