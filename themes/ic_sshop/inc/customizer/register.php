<?php

function sshop_customize_register( $wp_customize ) {
    $wp_customize->add_setting(
        'body_background_color_1',
        array( 'default' => '#F9F3ED', )
    );

    $wp_customize->add_setting(
        'body_background_color_2',
        array( 'default' => '#F9F3ED', )
    );

    $wp_customize->add_setting(
        'top_bar_text_color',
        array( 'default' => '#000000', )
	);

    $wp_customize->add_setting(
        'site_header_text_color',
        array( 'default' => '#FFFFFF', )
    );

    $wp_customize->add_setting(
        'search_bar_background_color',
        array( 'default' => '#000000', )
    );

    $wp_customize->add_setting(
        'off_canvas_menu_background_color',
        array( 'default' => '#000000', )
    );

    $wp_customize->add_setting(
        'site_footer_background_color_1',
        array( 'default' => '#000000', )
    );

    $wp_customize->add_setting(
        'site_footer_background_color_2',
        array( 'default' => '#000000', )
	);

    $wp_customize->add_setting(
        'site_footer_border_color',
        array( 'default' => '#999999', )
	);

    $wp_customize->add_setting(
        'site_footer_text_color',
        array( 'default' => '#FFFFFF', )
	);

    // Finally, add controls
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_background_color_1', array(
        'label'     => 'Body - Background Color 1',
        'section'   => 'colors',
        'settings'  => 'body_background_color_1',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_background_color_2', array(
        'label'     => 'Body - Background Color 2',
        'section'   => 'colors',
        'settings'  => 'body_background_color_2',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'top_bar_text_color', array(
        'label'     => 'Top Bar - Text Color',
        'section'   => 'colors',
        'settings'  => 'top_bar_text_color',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_header_text_color', array(
        'label'     => 'Site Header - Text Color',
        'section'   => 'colors',
        'settings'  => 'site_header_text_color',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'search_bar_background_color', array(
        'label'     => 'Search Bar - Background Color',
        'section'   => 'colors',
        'settings'  => 'search_bar_background_color',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'off_canvas_menu_background_color', array(
        'label'     => 'Sliding Menu - Background Color',
        'section'   => 'colors',
        'settings'  => 'off_canvas_menu_background_color',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_footer_background_color_1', array(
        'label'     => 'Site Footer - Background Color 1',
        'section'   => 'colors',
        'settings'  => 'site_footer_background_color_1',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_footer_background_color_2', array(
        'label'     => 'Site Footer - Background Color 2',
        'section'   => 'colors',
        'settings'  => 'site_footer_background_color_2',
	)));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_footer_border_color', array(
        'label'     => 'Site Footer - Border Color',
        'section'   => 'colors',
        'settings'  => 'site_footer_border_color',
    )));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_footer_text_color', array(
        'label'     => 'Site Footer - Text Color',
        'section'   => 'colors',
        'settings'  => 'site_footer_text_color',
    )));
}
add_action( 'customize_register', 'sshop_customize_register' );