<?php

/**
* Register any menus we need.
*/
function sshop_register_menus(){

   // Register our custom menus
   register_nav_menu( 'sites-menu', __( 'Sites Menu', 'sshop' ) );
   register_nav_menu( 'top-bar-menu', __( 'Top Bar Menu', 'sshop' ) );
   register_nav_menu( 'top-bar-menu-logged-in', __( 'Top Bar Menu - Logged In', 'sshop' ) );
   register_nav_menu( 'header-menu', __( 'Header Menu', 'sshop' ) );
   register_nav_menu( 'off-canvas-menu', __( 'Off Canvas Menu', 'sshop' ) );
   register_nav_menu( 'footer-menu', __( 'Footer Menu', 'sshop' ) );
}
add_action( 'after_setup_theme', 'sshop_register_menus' );