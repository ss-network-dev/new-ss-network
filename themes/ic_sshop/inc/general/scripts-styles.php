<?php

/**
 * Load theme assets
 */
function sshop_load_theme_assets() {
	// Load FontAwesome
	wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.7.2/js/all.js', NULL, '5.7.2', false );

	// Load SlickJS styles ONLY on the home page
	if ( is_home() ) {
	wp_enqueue_style( 'slick-styles', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
	}

	// Load in Google Fonts
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Overpass:400,700' );

	// Load main CSS file
	wp_enqueue_style( 'sshop-styles', trailingslashit( get_template_directory_uri() ) . 'css/main.css', array( 'woocommerce-layout', 'woocommerce-smallscreen', 'woocommerce-general' ) );

	// Load SlickJS on the home page ONLY
	if ( is_home() ) {
		wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), NULL, true );
	}

	// Load main JS file
	wp_enqueue_script( 'sshop-scripts', trailingslashit( get_template_directory_uri() ) . 'js/scripts.js', array( 'jquery' ), NULL, true );
}
add_action( 'wp_enqueue_scripts', 'sshop_load_theme_assets' );