<?php

function sshop_register_sidebars() {
    register_sidebar( array(

        'name'          => __( 'Shop Sidebar', 'sshop' ),
        'id'            => 'shop-sidebar',
        'description'   => __( 'Widgets in this area will be shown on the Shop pages', 'sshop' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sshop_register_sidebars' );