<?php

// All theme supports go here
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-lightbox' );