<?php

/**
 * Show cart contents / total Ajax
 */
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a href="<?php echo wc_get_cart_url(); ?>" class="button cart-customlocation">
		<span class="fas fa-shopping-cart"></span>
		<span class="button-text"><?php echo $woocommerce->cart->get_cart_total(); ?></span>
	</a>
	<?php

	$fragments['a.cart-customlocation'] = ob_get_clean();

	return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );