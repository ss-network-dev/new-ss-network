<?php 

function sshop_shop_page_title( $page_title ) {
    if( 'Shop' == $page_title) {
        return "All Clips/Videos";
    }
    else {
        return $page_title;
    }
}
add_filter( 'woocommerce_page_title', 'sshop_shop_page_title');