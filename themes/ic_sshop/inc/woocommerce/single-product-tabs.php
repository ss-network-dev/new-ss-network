<?php

function sshop_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['additional_information'] );

    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'sshop_remove_product_tabs', 98 );
