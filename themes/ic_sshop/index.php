<?php

get_header();
?>

<main id="site-main" class="main">
	<div class="wrap">
	<?php
		if( have_posts() ) {
			while( have_posts() ) {
				the_post();

				the_content();
			}
		}
	?>
	</div>
</main>

<?php
get_footer();