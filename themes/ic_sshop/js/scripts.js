jQuery(function ($) {
	$('#off-canvas-trigger').on('click touch', function () {
		$('.off-canvas-container').toggleClass('active');
	});

	$('#off-canvas-close').on('click touch', function () {
		$('.off-canvas-container').toggleClass('active');
	});

	$('#search-trigger').on('click touch', function () {
		$('.search-form-container').slideToggle();
	});

	// Home Page Slider
	if($('body').hasClass('home')) {
		$('.home-slider').slick({
			fade: true,
			prevArrow: '<div class="slider-arrow slider-arrow-prev"><span class="fa fa-angle-left"></span><span class="sr-only">Prev</span></div>',
			nextArrow: '<div class="slider-arrow slider-arrow-next"><span class="fa fa-angle-right"></span><span class="sr-only">Next</span></div>',
		});
	}

	$('.ajax-cart-button').on('click touch', function (event) {
		event.preventDefault();
		
		$thisButton = $(this);
		$ID			= $thisButton.data('product-id');

		var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: $ID,
            product_sku: '',
            quantity: 1,
		};
		
		$(document.body).trigger('adding_to_cart', [$thisButton, data]);

		$.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function (response) {
                $thisButton.removeClass('added').addClass('loading');
            },
            complete: function (response) {
                $thisButton.addClass('added').removeClass('loading');
            },
            success: function (response) {
 
                if (response.error & response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                    $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisButton]);
                }
            },
        });
 
        return false;
	});
});