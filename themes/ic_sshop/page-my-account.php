<?php

// Template Name: My Account

get_header();
?>

<main id="site-main" class="main">
    <div class="wrap">
        <h1 class="headline">Dashboard</h1>
        <?php
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post();

                the_content();
            }
        }
        ?>
    </div>
</main>

<?php
get_footer();