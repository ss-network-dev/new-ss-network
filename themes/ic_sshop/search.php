<?php
/**
 * The template for displaying search results pages.
 */

get_header(); ?>

    <main id="main" class="main" role="main">
        <div class="wrap">

		<div class="sidebar"><?php dynamic_sidebar( 'shop-sidebar' ); ?></div>

        <div class="woo-content">
            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <h1 class="page-title">
                        <?php
                            /* translators: %s: search term */
                            printf( esc_attr__( 'Search Results for: %s', 'storefront' ), '<span>' . get_search_query() . '</span>' );
                        ?>
                    </h1>
                </header><!-- .page-header -->
                <div class="front-products">

            <?php
                while ( have_posts() ) :
                    the_post();
            ?>

                <?php
                    get_template_part( 'template-parts/product', 'front' );
                
                endwhile;

            else :

                get_template_part( 'content', 'none' );

            endif;
            ?>
                </div>
            </div>
        </div>

    </main><!-- #main -->

<?php
get_footer();