<?php
	$thumb_url = get_the_post_thumbnail_url();
	$thumb_low = strtolower( $thumb_url );

	// Check if we have a GIF or not
	if ( false === strpos( $thumb_low, 'gif' ) ) {
		$thumb_size = 'front-product-image';
	} else {
		$thumb_size = 'full';
	}

	$vendor_name = get_the_author_meta( 'user_nicename' );
	$vendor_id = get_the_author_meta( 'id' );
	$vendor = get_user_by( 'id', $vendor_id );
	$vendor_shop_link = site_url( WC_Vendors::$pv_options->get_option( 'vendor_shop_permalink' ) . '/' . $vendor->pv_shop_slug );
	$sold_by_label = "Sold By: " . $vendor_name;

	$product = wc_get_product( get_the_ID() );
?>

<article id="product-<?php the_ID(); ?>" <?php post_class( 'front-product' ); ?>>
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail( $thumb_size ); ?>
		<span class="product-price">$<?php echo esc_html( $product->get_price() ); ?></span>
	</a>
	<p class="sold-by">
		<span>Sold by: </span>
		<a href="<?php echo esc_url( $vendor_shop_link ); ?>"><?php echo esc_html( $vendor_name ); ?></a>
	</p>
	<a class="button ajax-cart-button" href="/?add-to-cart=<?php the_ID(); ?>" data-product-id="<?php the_ID(); ?>">Add to Cart</a>
</article>