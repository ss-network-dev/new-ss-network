<?php

// Template Name: Vendors List

get_header();
?>

<main id="site-main" class="main">
	<h1 class="headline">All Models</h1>
	<?php echo do_shortcode( '[wcv_vendorslist]' ); ?>
</main>

<?php
get_footer();