<?php

get_header();
?>

<main id="site-main" class="main">
	<div class="wrap">
		<div class="sidebar"><?php dynamic_sidebar( 'shop-sidebar' ); ?></div>
		<div class="woo-content">
			<?php woocommerce_content(); ?>
		</div>
	</div>
</main>

<?php
get_footer();